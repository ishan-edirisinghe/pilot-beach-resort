<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pilot-beach-resort');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u;B>,Fs7%O?f+2Ip?H%-?51| E<)g(S?v`g89iA1Z8WLrU|_ZFC<J?!&k9gp} (r');
define('SECURE_AUTH_KEY',  'd52dp9y2q:ML>o*hMvbqW[r[x5.5A4aTSzt5eXz>mlc;|Ps<*qIlDewC98O?l&R(');
define('LOGGED_IN_KEY',    'D<uM1YYnuK3i9VWZCt5rk]+=F/z(X[z:@/0Ka53mOQik8UJ~/xyG3bB1-!X({j?!');
define('NONCE_KEY',        'b@skbP.&b|tOqZJe=b.OD+s^{ET])R4yN4HbEiMe>:53^`#p2q$l@hKE5T)|31|.');
define('AUTH_SALT',        'y(zyxr:|?HMX#Bk-e{eP6@<(W4^*Uc1LFLkJN7/:fl&+k8)FOJksEMOv5r*Ev9xj');
define('SECURE_AUTH_SALT', '3QVH ;GQl5r) %VIq#K#)=|tzcX>t-p1Qrd;u0qt!Oa#YH:SYgEh`;Y8~%%E0bSE');
define('LOGGED_IN_SALT',   'R+BI&7A11=p,He29{sp0s&A;:pl8a(fS$$/P|?UyIm!sN`w^y|3?8HZi+J,Fv5d4');
define('NONCE_SALT',       'o9_V-Qx10Mk)hByJZzk@+cBt<~g~oJZ~)J)!=u.tpBNoLKNt,a97dZ1dK2:jvXz-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wppb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
