var $ = jQuery.noConflict();
$(document).ready(function(){
    heroBanner();
    offerPopup();
    scrollToTop();
    smoothDown();

 
});


function offerPopup(){
    setTimeout(function() {
        $('#pop_content').show();
    }, 2000);
    $('#popClose').click(function(){
        $('#pop_content').hide();
    });
}

function heroBanner(){
    var window_height = $(window).height();
	$("#heroBanner").css("height", window_height);
}


function scrollToTop(){
    $(window).bind("scroll", function () {
        if ($(this).scrollTop() > 100) {
            $('#toTop').fadeIn(400);
        } else {
            $('#toTop').fadeOut(400);
        }         
    });

    $('#toTop').click(function(){
        $('html, body').animate({scrollTop: '0px'}, 1000);
     }); 
}


function smoothDown(){
    $('a').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 800);
        return false;
    });
    
}