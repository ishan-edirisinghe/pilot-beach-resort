<?php 
    $add_hero_banner = get_field('add_hero_banner');
    $banner = get_field('banner');
    $caption = get_field('caption');
?>
<section id="heroBanner" class="container-fluid" style="background:url(<?= $banner['url']; ?>">
    <div class="banner_caption">
        <div class="container">
            <div class="hero_title">
                <h2><?= $caption; ?></h2>
            </div>
            <a href="#introText" class="small_text">Scroll down for more <i class="arrow_icon"></i></a>

            <div id="pop_content">
                <span id="popClose"><i class="fas fa-times"></i></span>
                <p class="pop_cat">News & Offers</p>
                <h3>3 Reasons to back</h3>
                <ul>
                    <li>Book for 2018</li>
                    <li>Free Upgrade to Half Board</li>
                    <li>Free Transfer</li>
                </ul>
                <a href="" class="theme_btn">Learn More</a>
            </div>
        </div>
    </div>
</section>