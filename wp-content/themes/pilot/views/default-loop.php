<?php
    get_template_part('views/hero-banner');

	if ( have_rows ('layout_options') ){
		while ( have_rows ('layout_options') ){
			the_row();
			switch ( get_row_layout() ){
				case 'single_row_text':
					get_template_part('views/single-row-text');
					break;
				case 'image_with_text_row':
					get_template_part('views/image-with-text-row');
					break;				
	
				default:
					echo "Unknown Layout";
					break;
			}
		}
	}