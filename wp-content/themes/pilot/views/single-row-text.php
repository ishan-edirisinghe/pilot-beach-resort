<?php 
    $title = get_sub_field('title');
    $content = get_sub_field('content');
    $button_label = get_sub_field('button_label');
    $button_target = get_sub_field('button_target');
?>
<section id="introText" class="container-fluid">
    <div class="container">
        <div class="text_wrap">
            <h2><?= $title; ?></h2>
            <p><?= $content; ?></p>
            <a href="<?= $button_target; ?>" class="theme_btn"><?= $button_label; ?></a>    
        </div>
    </div>
</section>