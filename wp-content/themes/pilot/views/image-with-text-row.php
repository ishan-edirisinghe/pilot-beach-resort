<?php
    $image_show_in_right = get_sub_field('image_show_in_right');
    $image = get_sub_field('image');
    $title = get_sub_field('title');
    $description = get_sub_field('description');
    $button_label = get_sub_field('button_label');
    $button_color = get_sub_field('button_color');
    $button_target = get_sub_field('button_target');
?>
<section id="featuredSec" class="container-fluid no_padd">
    <div class="col-sm-12 col-md-8 no_padd fea_image <?php if($image_show_in_right == 1){echo 'img_show_right';} ?>">
        <img src="<?= $image['url'] ?>" alt="">
    </div>
    <div class="col-sm-12 col-md-4 fea_info">
        <h3><?= $title; ?></h3>
        <p><?= $description; ?></p>
        <a href="<?= $button_target; ?>" style="background:<?= $button_color; ?>" class="theme_btn"><?= $button_label; ?> <i class="arrow_icon"></i></a>
    </div>
</section>