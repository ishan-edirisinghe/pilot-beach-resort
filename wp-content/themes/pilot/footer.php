    <section id="footer"class="conainer-fluid">
        <div id="footerMenu" class="row">
            <div class="container">
            <?php
                $args = array
                (
                    'theme_location'  => 'Footer Menu',
                    'menu_class'      => 'nav navbar-nav'
                );
                wp_nav_menu($args);
            ?>
            <span id="toTop"><i class="fas fa-arrow-up"></i></span>
            </div>
        </div>
        <div id="footerLogo" class="container-fluid">
            <div class="container">
                <ul class="footer_logo">
                    <?php
                        if( have_rows('footer_logo', 'options') ):
                            while ( have_rows('footer_logo', 'options') ) : the_row();
                                $logo = get_sub_field('logo');
                        ?>
                            <li><img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>"></li>
                        <?php
                            endwhile;
                            else :
                        endif;
                    ?>
                </ul>
            </div>
        </div>
        <div id="copyRights" class="container-fluid">
            <p><?php echo date(Y); ?> © Pilot Beach 5 star Resort in Crete | Website by Hotel Web Agency</p>
        </div>
    </section>
    
    
    <?php wp_footer(); ?>
    </body>
</html>