<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <?php wp_head(); ?>
  </head>
  <body>
    <?php $logo = get_field('logo', 'options') ?>
    <nav class="pushy pushy-left" data-focus="#first-link">
        <div class="pushy-content">
          <?php
              $args = array
              (
                  'theme_location'  => 'Main Menu',
                  'menu_class'      => 'nav navbar-nav'
              );
              wp_nav_menu($args);
          ?>
        </div>
    </nav>   
    <div class="site-overlay"></div>
    <section id="header" class="container-fluid no_padd">
      <!-- <div class="container"> -->
        <div class="col-xs-4 col-sm-5 col-md-4 no_padd">
          <span class="menu_btn menu-btn">Menu</span>
          <span class="lang" onclick="openNav()">EN <i class="arrow_icon"></i></span>
        </div>
        <div class="col-xs-4 col-sm-2 col-md-4 no_padd brand">
          <a href="<?php echo site_url(); ?>"><img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>"></a>
        </div>
        <div class="col-xs-4 col-sm-5 col-md-4 no_padd">
          <a href="" class="theme_btn">Book Now</a>
          <ul class="head_social">
            <li><a href=""><i class="fas fa-phone"></i></li>
            <li><a href=""><i class="fas fa-envelope"></i></li>
            <li><a href=""><i class="fas fa-map-marker-alt"></i></li>
            <li><a href=""><i class="fas fa-camera"></i></a></li>
          </ul>          
        </div>
      <!-- </div> -->
    </section>