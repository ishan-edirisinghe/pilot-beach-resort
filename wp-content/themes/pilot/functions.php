<?php
//Load jQuery
function jQuery_script(){
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_site_url() . '/wp-includes/js/jquery/jquery.js', array(), '1.0');

    wp_enqueue_script('jquery');
}
add_action("init", "jQuery_script");


function load_assets(){
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.7');
    wp_enqueue_style('pushy-style', get_template_directory_uri() . '/css/pushy.css', false, '1.0.0');
    wp_enqueue_style('main-style', get_template_directory_uri() . '/css/styles.css', false, '1.0.0');
    
    //wp_enqueue_script('jquery-min', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', array('jquery'), '1.12.4', true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
    wp_enqueue_script('pushy-js', get_template_directory_uri() . '/js/pushy.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('main-js', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'load_assets');


//Register nav menus
function register_my_menus() {
    register_nav_menu('Main Menu',__( 'Main Menu' ));
    register_nav_menu('Footer Menu',__( 'Foter Menu' ));
}
add_action( 'init', 'register_my_menus' );

//ACF Global settings section
if( function_exists('acf_add_options_page') ) 
{
    // add parent
    $parent = acf_add_options_page(array(
        'page_title'    => 'Global Settings'
    ));
    
    // add sub pages
    acf_add_options_sub_page(array(
        'page_title'    => 'General Settings',
        'parent_slug'   => $parent['menu_slug']
    ));
}

